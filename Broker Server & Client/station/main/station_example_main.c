/* WiFi station Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include <lwip/sockets.h>
#include "lwip/err.h"
#include "lwip/sys.h"
#include <esp_log.h>
#include <errno.h>
#include "sdkconfig.h"


/* The examples use WiFi configuration that you can set via 'make menuconfig'.

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/
#define EXAMPLE_ESP_WIFI_SSID      	"Lab 1"
#define EXAMPLE_ESP_WIFI_PASS      	"MY@w1y@@"
#define EXAMPLE_ESP_MAXIMUM_RETRY  	10
//#define PORT_NUMBER 				5000
#define DEVICE_IP          "192.168.1.50"
#define DEVICE_GW          "192.168.1.1"	// The Gateway address where we wish to send packets.
#define DEVICE_NETMASK     "255.255.255.0"	// The netmask specification.

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;

/* The event group allows multiple bits for each event, but we only care about one event 
 * - are we connected to the AP with an IP? */
const int WIFI_CONNECTED_BIT = BIT0;

static const char *TAG = "wifi station";

static int s_retry_num = 0;

//***************CLIENT*******************************
void socketClient(void *ignore)
{
	printf("start");
	int sock = socket(AF_INET, SOCK_STREAM, 0);

	printf("socket: rc: %d", sock);
	struct sockaddr_in serverAddress;
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr=inet_addr("192.168.1.146");
	serverAddress.sin_port = htons(5000);
	printf("try to connect");
	//inet_pton(AF_INET, "192.168.1.69", &serverAddress.sin_addr.s_addr);
	while(1)
	{
		int rc = connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
		if(rc<0){
		//printf("connect rc: %d", rc);
		continue;
		}

		char *data = "B,1\r\n";
		rc = send(sock, data, strlen(data), 0);
		printf("send: rc: %d", rc);

		rc = close(sock);
		printf("close: rc: %d\n", rc);
		vTaskDelay(1000/portTICK_PERIOD_MS);
	}
	vTaskDelete(NULL);
}
//***********************SERVER******************************

void socket_server(void *ignore)
{
	struct sockaddr_in clientAddress;
	struct sockaddr_in serverAddress;

	// Create a socket that we will listen upon.
	int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock < 0)
	{
		printf( "socket: %d %s", sock, strerror(errno));
		//goto END;
	}

	// Bind our server socket to a port.
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(5000);
	int rc  = bind(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
	if (rc < 0)
	{
		printf("bind: %d %s", rc, strerror(errno));
		//goto END;
	}

	// Flag the socket as listening for new connections.
	rc = listen(sock, 5);
	if (rc < 0) {
		printf( "listen: %d %s", rc, strerror(errno));
		//goto END;
	}

	while (1)
	{
		// Listen for a new client connection.
		socklen_t clientAddressLength = sizeof(clientAddress);
		int clientSock = accept(sock, (struct sockaddr *)&clientAddress, &clientAddressLength);
		if (clientSock < 0)
		{
			printf( "accept: %d %s", clientSock, strerror(errno));
			//goto END;
		}

		// We now have a new client ...
		int total =	10*1024;
		int sizeUsed = 0;
		char *data = malloc(total);

		// Loop reading data.
		while(1)
		{
			ssize_t sizeRead = recv(clientSock, data + sizeUsed, total-sizeUsed, 0);
			if (sizeRead < 0)
			{
				printf("recv: %d %s", sizeRead, strerror(errno));
				//goto END;
			}
			if (sizeRead == 0)
			{
				break;
			}
			sizeUsed += sizeRead;
		}
		printf( "Data read (size: %d) was: %.*s", sizeUsed, sizeUsed, data);

		free(data);
		// Finished reading data.
		//free(data);

		close(clientSock);

	}
	//END:
	//vTaskDelete(NULL);
}
//***********************************************************
void dummy()
{
	int count=0;
	while(1)
	{
		count++;
		vTaskDelay(1000/portTICK_PERIOD_MS);
	}
	vTaskDelete(NULL);
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "got ip:%s",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        {
            if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
                esp_wifi_connect();
                xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
                s_retry_num++;
                ESP_LOGI(TAG,"retry to connect to the AP");
            }
            ESP_LOGI(TAG,"connect to the AP fail\n");
            break;
        }
    default:
        break;
    }
    return ESP_OK;
}

void wifi_init_sta()
{
    s_wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();

    tcpip_adapter_dhcpc_stop(TCPIP_ADAPTER_IF_STA); // Don't run a DHCP client
    tcpip_adapter_ip_info_t ipInfo;
    inet_pton(AF_INET, DEVICE_IP, &ipInfo.ip);
    inet_pton(AF_INET, DEVICE_GW, &ipInfo.gw);
    inet_pton(AF_INET, DEVICE_NETMASK, &ipInfo.netmask);
    tcpip_adapter_set_ip_info(TCPIP_ADAPTER_IF_STA, &ipInfo);

    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL) );

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");
    ESP_LOGI(TAG, "connect to ap SSID:%s password:%s",
             EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
}

void app_main()
{
    esp_err_t ret = nvs_flash_init();		//Initialize NVS
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    wifi_init_sta();

    xTaskCreate(socketClient,"socketClient",4000,NULL,2,NULL);
    xTaskCreate(dummy,"dummy",1000,NULL,1,NULL);
    xTaskCreate(socket_server,"socket_server",3000,NULL,3,NULL);
   // xTaskCreate(socket_server,"socket_server",3000,NULL,3,NULL);

}
